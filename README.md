# Dev Environment for EXT:extbase_session_entities

This repository provides scripts, data and configuration
to setup a full TYPO3 CMS development environment.

Also it contains an **example extension**, which implements the classes and methods
from [EXT:extbase_session_entities](https://bitbucket.org/t--3/extbase_session_entities)

## Components

* Vagrantfile to provide easy to set up dev environment
* composer.json with [typo3/minimal](https://packagist.org/packages/typo3/minimal) configuration
* Example extension (located in `/example`)
* Submodule to actual main extension (located in `/src`)
* AdditionalConfiguration.php for debug setup (located in `/html/public/typo3conf/AdditionalConfiguration.php`)
* MySQL dump (gets imported automatically on provisioning of vagrant box)


## Requirements

* Virtual Box
* Vagrant
* Git

## Installation

1. Clone this repository instead of the extension repo. It contains the extension
using a git submodule in folder `/src`. To fetch also the submodules use:
    ```
    git clone --recurse-submodules https://bitbucket.org/t--3/extbase_session_entities-dev.git
    ```

2. Run `vagrant up`. That's it.


### Credentials

After successful vagrant up you can call TYPO3 with:

* Frontend: http://extbase-session-entities.vagrant
* Backend: http://extbase-session-entities.vagrant/typo3
    * Username: `admin`
    * Password: `password` (also for install tool)

**More credentials:**

* Vagrant SSH login: `vagrant` / `vagrant` (or `vagrant ssh`on CLI)
* MySQL login: `root` / *no password*


For MySQL the client [Adminer](https://www.adminer.org) is also installed and available under:

http://extbase-session-entities.vagrant/adminer 
