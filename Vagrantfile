# -*- mode: ruby -*-
# vi: set ft=ruby :

if Vagrant::Util::Platform.windows? then
    unless Vagrant.has_plugin?("vagrant-winnfsd")
        system "vagrant plugin install vagrant-winnfsd"
    end
end
unless Vagrant.has_plugin?("vagrant-bindfs")
    system "vagrant plugin install vagrant-bindfs"
end
unless Vagrant.has_plugin?("vagrant-hostmanager")
    system "vagrant plugin install vagrant-hostmanager"
end
unless Vagrant.has_plugin?("vagrant-triggers")
    system "vagrant plugin install vagrant-triggers"
end

Vagrant.configure("2") do |config|
    # Base configuration
    config.vm.box = "ArminVieweg/ubuntu-xenial64-lamp"

    staticIpAddress = "192.168.22.38"
    httpPortForwardingHost = "8088"
    config.vm.hostname = "extbase-session-entities.vagrant"
    
    if Vagrant::Util::Platform.windows? then
        config.trigger.after :up, :good_exit => [0, 1] do
            run "explorer http://#{config.vm.hostname}"
        end
    end

	config.ssh.insert_key = false

    config.vm.network "private_network", type: "dhcp"
    config.vm.provider "virtualbox" do |vb|
        vb.memory = 4096
        vb.cpus = 2
    end

    # Synchronization
    config.vm.synced_folder ".", "/vagrant", disabled: true
    config.vm.synced_folder ".", "/var/nfs", type: "nfs"

    config.bindfs.bind_folder "/var/nfs", "/vagrant",
        perms: "u=rwX:g=rwX:o=rD"

    config.bindfs.bind_folder "/var/nfs/example", "/var/www/example",
        perms: "u=rwX:g=rwX:o=rD", force_user: "vagrant", force_group: "www-data"

    config.bindfs.bind_folder "/var/nfs/src", "/var/www/src",
            perms: "u=rwX:g=rwX:o=rD", force_user: "vagrant", force_group: "www-data"

    # Hostmanager
    config.hostmanager.enabled = true
    config.hostmanager.manage_host = true
    config.hostmanager.manage_guest = true
    config.hostmanager.ignore_private_ip = false
    config.hostmanager.include_offline = true

    config.vm.define "default" do |node|
        node.vm.network :private_network, ip: staticIpAddress
        node.vm.network :forwarded_port, guest: 80, host: httpPortForwardingHost
    end

    # Provider Scripts
    # Run always
    config.vm.provision "shell", run: "always", name: "composer-update", inline: <<-SHELL
        cd ~
        composer self-update --no-progress
    SHELL

    # Run once (install TYPO3 after composer install)
    config.vm.provision "shell", run: "once", name: "typo3-install", inline: <<-SHELL
        sed -i s,/var/www/html,/var/www/html/public,g /etc/apache2/sites-available/000-default.conf
        service apache2 restart

        cp -Rf /vagrant/html/* /var/www/html

        cd /var/www/html
        composer install --no-progress

        echo "Installing TYPO3 CMS..."

        vendor/bin/typo3cms install:setup --force --database-user-name "root" --database-user-password "" --database-host-name "localhost" --database-name "typo3" --database-port "3306" --database-socket "" --admin-user-name "admin" --admin-password "password" --site-name "T3 Dev Environment" --site-setup-type "site" --use-existing-database 0
        vendor/bin/typo3cms cache:flush
        composer run sql-import

        echo "Fixing permissions..."
        chmod 2775 . ./public/typo3conf ./public/typo3conf/ext
        chown -R vagrant .
        chgrp -R www-data .

        printf "\ncd /var/www/html" >> /home/vagrant/.bashrc

        echo "Done. Happy coding!"
    SHELL

    # Run once (Add /adminer alias)
    config.vm.provision "shell", run: "once", name:"adminer-install", inline: <<-SHELL
        echo "Installing adminer..."
        composer require vrana/adminer -d /home/vagrant/.composer/ -o --no-progress
        ln -s /home/vagrant/.composer/vendor/vrana/adminer/adminer /var/www/adminer

        echo -e "Alias /adminer \"/var/www/adminer/\"\n<Directory \"/var/www/adminer/\">\nOrder allow,deny\nAllow from all\nRequire all granted\n</Directory>" > /etc/apache2/conf-available/adminer.conf
        a2enconf adminer
        echo "Restarting apache2..."
        service apache2 restart
    SHELL

end
