<?php
namespace T3\Example\Controller;

/*  | This extension is made with love for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018 Armin Vieweg <armin@v.ieweg.de>
 */

/**
 * Class OrderController
 */
class OrderController extends \T3\ExtbaseSessionEntities\Mvc\AbstractSessionEntityController
{
    /**
     * @var \T3\Example\Domain\Repository\OrderRepository
     * @inject
     */
    protected $orderRepository;

    /**
     * @var \SJBR\StaticInfoTables\Domain\Repository\CountryRepository
     * @inject
     */
    protected $countryRepository;

    /**
     * @param \T3\Example\Domain\Model\Order $order
     * @return void
     *
     * @ignorevalidation $order
     */
    public function step1Action(\T3\Example\Domain\Model\Order $order = null)
    {
        if (null === $order) {
            /** @var \T3\Example\Domain\Model\Order $order */
            $order = $this->objectManager->get(\T3\Example\Domain\Model\Order::class);
            $order->prepare();
        }

        \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($order);
        $this->view->assign('order', $order);
    }

    /**
     * @param \T3\Example\Domain\Model\Order $order
     * @return void
     *
     * @validate $order \T3\ExtbaseSessionEntities\Mvc\PartialObjectValidator(validate="firstName,name")
     */
    public function step2Action(\T3\Example\Domain\Model\Order $order)
    {
        $this->updateSessionEntity($order, $this->orderRepository);

        \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($order);

        $this->view->assign('order', $order);
    }

    /**
     * @param \T3\Example\Domain\Model\Order $order
     * @return void
     *
     * @validate $order \T3\ExtbaseSessionEntities\Mvc\PartialObjectValidator(validate="products")
     */
    public function step3Action(\T3\Example\Domain\Model\Order $order)
    {
        $this->updateSessionEntity($order, $this->orderRepository);

        \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($order);

        $this->view->assign('countries', $this->countryRepository->findAll());
        $this->view->assign('order', $order);
    }

    /**
     * @param \T3\Example\Domain\Model\Order $order
     * @return void
     *
     * @validate $order \T3\ExtbaseSessionEntities\Mvc\PartialObjectValidator()
     */
    public function finalAction(\T3\Example\Domain\Model\Order $order)
    {
        $this->updateSessionEntity($order, $this->orderRepository);

        \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($order);
        $this->view->assign('order', $order);
    }

    /**
     * @param \T3\Example\Domain\Model\Order $order
     * @return void
     * @validate $order \T3\ExtbaseSessionEntities\Mvc\PartialObjectValidator()
     */
    public function saveAction(\T3\Example\Domain\Model\Order $order)
    {
        $this->orderRepository->add($order);
        $this->persistAll();
        $this->redirect('final', null, null, ['order' => $order]);
    }
}
