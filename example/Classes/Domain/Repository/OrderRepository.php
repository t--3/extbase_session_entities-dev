<?php
namespace T3\Example\Domain\Repository;

/*  | This extension is made with love for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018 Armin Vieweg <armin@v.ieweg.de>
 */

/**
 * Order Repository
 */
class OrderRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
}
