<?php
namespace T3\Example\Domain\Model;

/*  | This extension is made with love for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018 Armin Vieweg <armin@v.ieweg.de>
 */

/**
 * Product Model
 */
class Product extends \T3\ExtbaseSessionEntities\Mvc\AbstractSessionEntity
{
    /**
     * @var string
     * @validate NotEmpty
     */
    protected $name = '';

    /**
     * @var int
     * @validate NotEmpty
     * @validate Integer
     * @validate NumberRange(minimum=1, maximum=1000)
     */
    protected $quantity = 0;

    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return void
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }
}
