<?php
namespace T3\Example\Domain\Model;

/*  | This extension is made with love for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018 Armin Vieweg <armin@v.ieweg.de>
 */

/**
 * Order Model
 */
class Order extends \T3\ExtbaseSessionEntities\Mvc\AbstractSessionEntity
{
    /**
     * @var string
     * @validate NotEmpty
     */
    protected $name = '';

    /**
     * @var string
     * @validate NotEmpty
     */
    protected $firstName = '';

    /**
     * @var string
     * @validate NotEmpty
     */
    protected $iban = '';

    /**
     * @var \SJBR\StaticInfoTables\Domain\Model\Country|null
     * @validate NotEmpty
     */
    protected $country;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3\Example\Domain\Model\Product>
     */
    protected $products;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->initStorageObjects();
    }

    /**
     * Initializes all \TYPO3\CMS\Extbase\Persistence\ObjectStorage properties.
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->products = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string $firstName
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return void
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string $iban
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * @param string $iban
     * @return void
     */
    public function setIban($iban)
    {
        $this->iban = $iban;
    }

    /**
     * @return \SJBR\StaticInfoTables\Domain\Model\Country|null
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param \SJBR\StaticInfoTables\Domain\Model\Country $country
     * @return void
     */
    public function setCountry(\SJBR\StaticInfoTables\Domain\Model\Country $country = null)
    {
        $this->country = $country;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getProducts() : \TYPO3\CMS\Extbase\Persistence\ObjectStorage
    {
        return $this->products;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $products
     * @return void
     */
    public function setProducts(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $products)
    {
        $this->products = $products;
    }

    /**
     * @param Product $product
     * @return void
     */
    public function addProduct(Product $product)
    {
        $this->products->attach($product);
    }

    /**
     * @param Product $product
     * @return void
     */
    public function removeProduct(Product $product)
    {
        $this->products->detach($product);
    }
}
