<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Extbase Session Entities Example',
    'description' => 'Example extension, demonstrating EXT:extbase-session-entities',
    'category' => 'plugin',
    'author' => '',
    'author_email' => '',
    'state' => 'alpha',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0-dev',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-8.7.99',
            'static_info_tables' => '6.5.0-6.5.99',
            'extbase_session_entities' => '1.0.0-1.99.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
    'autoload' => [
        'psr-4' => [
            'T3\\Example\\' => 'Classes'
        ]
    ]
];
