<?php
defined('TYPO3_MODE') || die('Access denied.');

/*  | This extension is made with love for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018 Armin Vieweg <armin@v.ieweg.de>
 */

call_user_func(
    function () {
        // Configure Extbase Plugin
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'T3.Example',
            'Order',
            [
                'Order' => 'step1, step2, step3, final, save'
            ],
            [
                'Order' => 'step1, step2, step3, final, save'
            ]
        );
    }
);
