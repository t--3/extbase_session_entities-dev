
plugin.tx_example_order {
    view {
        templateRootPaths.0 = EXT:example/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_example_order.view.templateRootPath}
        partialRootPaths.0 = EXT:example/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_example_order.view.partialRootPath}
        layoutRootPaths.0 = EXT:example/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_example_order.view.layoutRootPath}
    }
    features {
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
}

# these classes are only used in auto-generated templates
plugin.tx_example._CSS_DEFAULT_STYLE (
    .f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    .tx-example table {
        border-collapse:separate;
        border-spacing:10px;
    }

    .tx-example table th {
        font-weight:bold;
    }

    .tx-example table td {
        vertical-align:top;
    }

    .typo3-messages .message-error {
        color:red;
    }

    .typo3-messages .message-ok {
        color:green;
    }

    form div {
        margin-bottom: 5px;
    }

    label span {
        display: inline-block;
        min-width: 80px;
        font-weight: bold;
    }

    div.links {
        margin: 20px 0;
    }
)
