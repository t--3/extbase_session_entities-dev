
plugin.tx_example_order {
    view {
        # cat=plugin.tx_example_order/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:example/Resources/Private/Templates/
        # cat=plugin.tx_example_order/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:example/Resources/Private/Partials/
        # cat=plugin.tx_example_order/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:example/Resources/Private/Layouts/
    }
}
