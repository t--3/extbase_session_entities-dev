<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:example/Resources/Private/Language/locallang_db.xlf:tx_example_domain_model_product',
        'label' => 'name',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
        'searchFields' => 'name,quantity',
        'iconfile' => 'EXT:example/Resources/Public/Icons/tx_example_domain_model_product.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'hidden, name, quantity, order',
    ],
    'types' => [
        '1' => [
            'showitem' => 'hidden, name, quantity, order',
        ],
    ],
    'columns' => [
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],

        'name' => [
            'exclude' => true,
            'label' => 'LLL:EXT:example/Resources/Private/Language/locallang_db.xlf:tx_example_domain_model_product.name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'quantity' => [
            'exclude' => true,
            'label' => 'LLL:EXT:example/Resources/Private/Language/locallang_db.xlf:tx_example_domain_model_product.quantity',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'int'
            ],
        ],
        'order' => [
            'exclude' => false,
            'label' => 'LLL:EXT:example/Resources/Private/Language/locallang_db.xlf:tx_example_domain_model_product.order',
            'config' => [
                'type' => 'passthrough'
            ],
        ],
    ],
];
