<?php
defined('TYPO3_MODE') || die('Access denied.');

/*  | This extension is made with love for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018 Armin Vieweg <armin@v.ieweg.de>
 */

call_user_func(
    function () {
        // Register extbase plugin (in backend)
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'T3.Example',
            'Order',
            'Order'
        );

        // Add static extension typoscript
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
            'example',
            'Configuration/TypoScript',
            'Extbase Session Entities'
        );

        // Allow new TCA tables
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages(
            'tx_example_domain_model_order'
        );
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages(
            'tx_example_domain_model_product'
        );
    }
);
